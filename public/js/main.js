
function init() {
  console.log("script initialized");
  // Code here;


(function(){
//Selectors
let navToggle = document.querySelector('.nav-toggle'); //Menu Button
let nav = document.querySelector('.main-nav'); // Navigation change Style from right to left here
let navItem = document.querySelectorAll('.main-nav-item'); //li in Navigation


doToggle = function(e) {
  nav.classList.toggle('expanded'); //Toggle Nav from right to left
}

navToggle.addEventListener('click', function(e) {
  doToggle();
})


for(i = 0; i < navItem.length; i++) {
  //Close Nav on Item Click
  navItem[i].addEventListener('click', function(e) {
    doToggle();
  })

}




}());

}

window.onload = init;
