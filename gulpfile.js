var gulp = require('gulp');
//Autoprefixer
const autoprefixer = require('gulp-autoprefixer');

gulp.task('default', () =>
  gulp.src('css/*.css')
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('css'))
);
